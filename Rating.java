import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

class User {
	// user rating vector
	ArrayList <Double> U;
	public User() {
		this.U = new ArrayList<>();
	}
}
public class Rating {
	//rating matrix
	ArrayList< ArrayList <Double> > R;
	
	public Rating() {
		this.R = new ArrayList<>();		
	}
	public Rating(String file_name) {
		this.R = new ArrayList<>();
		this.loadDataRaw(file_name);
	}

	public void loadDataRaw(String file_name) {
		// get the data
		File f = new File(file_name);

		try (Scanner scanner = new Scanner(f)){	
			while(scanner.hasNextLine()) {
				String[] tokens = scanner.nextLine().split("\n");
				// read line by line
				for(String i:tokens) {
					// split line
					String[] line = i.split("\\t");	// get data by split by TAB
					// add file text line as basket
					this.addRow(line);
				}	
			}
		} catch (FileNotFoundException ex) {
			System.out.println(ex);
		}			
	}
	
	private void addRow(String[] line) {
		ArrayList<Double> row = new ArrayList<>(line.length);
		for (String s:line) {
			row.add(Double.valueOf(s));
		}
		R.add(row);
	}
	
	public void printM() {
		String format = "%.2f";
		System.out.print("\t");
		for (int i=0; i < this.R.get(0).size(); i++) {
			System.out.print("R" + (i+1) + "\t");
		}
		System.out.print("\n");
		int i = 0;
		for (ArrayList<Double> a:this.R) {
			System.out.print("U" + (i+1) + "\t");
			i++;
			for (Double v:a) {
				System.out.printf(format+ "\t", v );
			}
			System.out.print("\n");
		}
		System.out.print("\n");
	}	
	// cosine similarity of two vectors
	// cosSim = sum(UxV)/(sqrt( sum( U^2 ))*sqrt( sum( V^2 ))
	public double cosineSim(ArrayList<Double> U, ArrayList<Double> V) {
		// can't compute if sizes are different
		if (U.size() != V.size())
			return -1;
		//
		double sumUV = 0d;
		double sumU = 0d;
		double sumV = 0d;
		for (int i=0; i<U.size(); i++) {
			sumUV += U.get(i) * V.get(i); 
			sumU += Math.pow(U.get(i), 2); 
			sumV += Math.pow(V.get(i), 2);
		}
		// calculate result
		return sumUV / ( Math.sqrt(sumU)*Math.sqrt(sumV) );
	}
	// use only R indexes as Users
	public double cosineSim(int u, int v) {
		ArrayList<Double> U = this.R.get(u);
		ArrayList<Double> V = this.R.get(v);
		return cosineSim(U, V);
	}
	// find similar user
	// return array index
	// give index of user U
	public int findSimilarUser(int u) {
		double max = -1;
		int out_index = -1;
		for (int i=0; i<this.R.size(); i++) {
			if (i != u) {
				double similarity = cosineSim( u , i );
				if (similarity > max) {
					max = similarity;
					out_index = i;
				}
			}
		}
		return out_index;
	}
	// find cosineSim for all user pairs
	// u and v are most similar
	public int[] cosSimAll() {
		double max = -1;
		int max_i = -1;
		int max_j = -1; 
		for (int i=0; i<this.R.size(); i++) {
			for (int j=i+1; j<this.R.size(); j++) {			
				double sim = cosineSim(i, j);
				if (sim > max) {
					max = sim;
					max_i = i;
					max_j = j;
				}
				String sim_out = String.format("%.4f", sim);  
				System.out.println("U"+(i+1) + "-"+ "U"+(j+1)+ " : " + (sim_out));				
			}
		}
		int u = max_i;
		int v = max_j;
		System.out.println("most similar users are: U"+(u+1)+" and U"+(v+1));
		int[] out = {u, v};
		return out;
	}
	// combine rating of two users
	// users are given by R indexes
	public ArrayList<Double> combineRatings(int u, int v) {
		ArrayList<Double> UV = new ArrayList<>();
		ArrayList<Double> U = this.R.get(u);
		ArrayList<Double> V = this.R.get(v);

		// combine ratings as average
		for (int i=0; i<U.size(); i++) {
			double average = (U.get(i)+V.get(i))/2;
			UV.add( average );
		}
		return UV;
	}
	// combine most similar to u (by index of R)
	public void combineAndPrint(int u) {
		int v = findSimilarUser(u);
		ArrayList<Double> UV = combineRatings(u, v);
		// print
		int N = this.R.get(0).size()-1;
		String format = "%.2f";
		System.out.print("\t");
		for (int i=0; i < N; i++) {
			System.out.print("R" + (i+1) + "\t");
		}
		System.out.print("\n");
		for (int i=0; i < N; i++) {			
			//
			ArrayList<Double> A = this.R.get(i);
			if (i == u)
				A = UV;
			if (i != v) {
				System.out.print("U" + (i+1) + "\t");
				for (int j=0; j < A.size(); j++) {
					System.out.printf(format+ "\t", A.get(j) );
				}
				System.out.print("\n");
			}
		}
		System.out.print("\n");
		
	}
}
